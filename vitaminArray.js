const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];

1.

let itemAvailable = items.map((item) => item.name);
console.log(itemAvailable);


2.

let containingVitaminC = items.filter((item) => item.contains === 'Vitamin C');
console.log(containingVitaminC);


3.

let containingVitaminA = items.filter((item) => {
    return item.contains.includes("Vitamin A");
}).map((item) => { return item.name; })

console.log(containingVitaminA);


4.

let vitaminFormat = items.reduce((item, current) => {
    current['contains'].split(', ').filter(vitamin => {
        if (item[vitamin] === undefined)
            item[vitamin] = [];
        item[vitamin].push(current['name']);
        return true;
    });
    return item;
}, {})

console.log(vitaminFormat);


5.

let sortedItemsBasedVitamin = items.sort((item1, item2) => {
    let count1 = item1.contains.split(',').length;
    let count2 = item2.contains.split(',').length;
    if (count1 > count2) {
        return -1;
    }
    else {
        return 1;
    }
});

console.log(sortedItemsBasedVitamin);
